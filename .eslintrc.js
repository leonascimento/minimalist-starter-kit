module.exports = {
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "amd": true,
    "jest": true
  },
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: [
    "react", "class-property"
  ],
  rules: {
    "linebreak-style": [
      "error",
      "unix"
    ],
    "react/jsx-filename-extension": [
      1,
      {
        "extensions": [".js", ".jsx"]
      }
    ]
  },
  extends: [
    "airbnb"
  ],
  settings: {
    "import/resolver": "webpack"
  }
};

