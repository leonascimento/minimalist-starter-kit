const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const styleLintPlugin = require('stylelint-webpack-plugin');

const resolve = source => path.resolve(__dirname, source);

const sassLoaderOptions = {
  loader: 'sass-loader',
  options: {
    includePaths: [resolve(__dirname, 'node_modules')],
    sourceMap: true
  }
};

const cssLoaders = process.env.NODE_ENV === 'development' ? {
  test: /\.scss$/,
  use: [
    { loader: 'style-loader' },
    { loader: 'css-loader' },
    sassLoaderOptions
  ]
} : {
  test: /\.scss$/,
  loader: ExtractTextPlugin.extract([
    'css-loader?sourceMap',
    sassLoaderOptions
  ])
};

module.exports = {
  entry: ['./src/js/index.js'],
  output: {
    filename: 'app.bundle.js',
    path: resolve('dist'),
    publicPath: '/'
  },
  resolve: {
    alias: {
      'app': resolve('src'),
      'styles': resolve('src/styles'),
      '$': resolve('node_modules')
    }
  },
  devtool: process.env.NODE_ENV === 'development' ? 'eval' : 'source-map',
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test:  /\.(jpe?g|png|jpg|gif|svg|wav)$/i,
        loader: 'file-loader',
      },
      {
        test: /\.css$/,
        use: [
          'css-loader'
        ]
      },
      cssLoaders
    ]
  },
  devServer: {
    historyApiFallback: true
  },
  plugins: [
    new styleLintPlugin({
      configFile: '.stylelintrc',
      context: 'src',
      files: '**/*.scss',
      failOnError: false,
      quiet: false,
    }),
    new ExtractTextPlugin('styles.css'),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html'
    }),
  ]
};
