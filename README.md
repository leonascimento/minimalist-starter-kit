# React Minimalist stater kit 

### Requirements

* node 8.11.3, Use the [nvm](https://github.com/creationix/nvm) is a good alternative.
* npm 5.6.0

### Installation

```
npm i
```

### Running the Project

```
npm start 
```

Access `http://localhost:8080/`

### Running the tests

```
npm run test
```

### Running the lint
```
npm run lint
```

### Running the Coverage 

```
npm run test-watch
```

### Building for Production

```
npm run production
```
